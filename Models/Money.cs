﻿using System;
namespace holaMundo.Models
{
    public class Money
    {
        public Money()
        {
        }
        public int id { get; set; }
        public string name { get; set; }
        public string abbr { get; set; }
        public double value { get; set; }
        public DateTime date { get; set; }
    }
}
