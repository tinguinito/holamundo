﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace holaMundo.Models
{
    public class MockMoney: IMoneyRepository
    {
       private List<Money> _moneyList;
        public MockMoney()
        {
            if(_moneyList == null){
                InciatialiceMoney();
            }
        }

        private void InciatialiceMoney() => _moneyList = new List<Money> {
            new Money { id = 1, name = "Dolar", abbr = "USD", value = 750 },
           new Money { id = 1, name = "Euro", abbr = "€", value = 750 },
            new Money { id = 1, name = "Pesos", abbr = "CLP", value = 750 }
            };

        public List<Money> GetAllMoney() => _moneyList;

        public Money GetMoneyById(int moneyId) => _moneyList.FirstOrDefault(m => m.id == moneyId);
    }


}
