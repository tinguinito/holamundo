﻿using System;
using System.Collections.Generic;

namespace holaMundo.Models
{
    public interface IMoneyNueva
    {
        List<Money> GetAllMoney();

        Money GetMoneyById(int pieId);
    }
}
