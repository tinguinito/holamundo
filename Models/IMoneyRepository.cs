﻿using System;
using System.Collections.Generic;

namespace holaMundo.Models
{
    public interface IMoneyRepository
    {
        List<Money> GetAllMoney();

        Money GetMoneyById(int pieId);
    }
}
