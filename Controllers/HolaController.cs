﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace holaMundo.Controllers
{
    public class HolaController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {

            var persona = new Person { Name = "Césartrrrrrrrrrr" };
            ViewData["Nombre"] = "Césaradadadads";
            return View(persona);
        }
    }

    public class Person {
        public string Name { get; set; }
    }

}
