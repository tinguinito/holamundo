﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using holaMundo.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace holaMundo.Controllers
{
    public class MoneyController : Controller

    {

        private readonly IMoneyRepository _moneyRepository;

        public MoneyController(IMoneyRepository moneyRepository) => _moneyRepository = moneyRepository;

        public IActionResult Index()
        {
            List<Money> moneyList = _moneyRepository.GetAllMoney().OrderBy(m => m.name).ToList();
            return View(moneyList);
        }
    }
}
